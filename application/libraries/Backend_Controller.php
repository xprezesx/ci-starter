<?php

class Backend_Controller extends MY_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();

        // Set the admin template
        $this->output->set_template(config_item('theme'));
        $this->load->library('form_validation');

        // Needed to callback functions in HMVC
        $this->form_validation->CI =& $this;
    }
}

/* End of file Backend_controller.php */
/* Location: ./application/libraries/Backend_controller.php */


