<?php
  
if ( ! function_exists('prepareURL')) 
{
    /**
     * Prepare URL fron string
     * 
     * @param string $sText
     * @return string
     */
    function prepareURL($sText)
    {  
        $sText = clearDiacritics($sText);
        $sText = strtolower($sText);
        $sText = str_replace(' ', '-', $sText);
        $sText = preg_replace('/[^0-9a-z\-\/]+/', '', $sText);
        $sText = preg_replace('/[\-]+/', '-', $sText);
        $sText = trim($sText, '-');

        return $sText;
    }
}

if ( ! function_exists('prepareImageName')) 
{
    /**
     * Prepare image name from string
     * 
     * @param string $sText
     * @return string
     */
    function prepareImageName($sText)
    {
        $sText = clearDiacritics($sText);
        $sText = strtolower($sText);
        $sText = str_replace(' ', '-', $sText);
        $sText = preg_replace('/[^0-9a-z\-._]+/', '', $sText);
        $sText = preg_replace('/[\-]+/', '-', $sText);
        $sText = trim($sText, '-');  

        return $sText;  
    } 
}

if ( ! function_exists('extension')) 
{
    /**
     * Get extension from string
     * 
     * @param string $file_name
     * @return string
     */
    function extension($file_name)
    {
        $ex = explode(".", $file_name);
        $i = (count($ex) - 1);

        return strtolower($ex[$i]);
    }
}