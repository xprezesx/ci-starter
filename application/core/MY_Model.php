<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Default CRUD action on CodeIgniter 3.0.0
 * 
 * @version    	0.1
 * @author     	Jacek Bednarek <jacek.bednarek1991@gmail.com>
 */
class MY_Model extends CI_Model
{
    protected $_table_name = '';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id';
    protected $_timestamp = FALSE;
    
    public $rules = array();
            
    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Get array data from POST
     * 
     * @param array $fields
     * @return array
     */
    public function array_from_post($fields) {
        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->input->post($field);
        }
        
        return $data;
    }
    
    /**
     * Default SELECT action from database by ID
     * 
     * @param int $id
     * @param boolean $single
     * @param boolean $num_rows
     * @param int $limitFrom
     * @param int $limitTo
     * @return mixed
     */
    public function get($id = NULL, $single = FALSE, $num_rows = FALSE, $limitFrom = NULL, $limitTo = '')
    {
        if($id != NULL)
        {
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->where($this->_table_name . '.' . $this->_primary_key, $id);
            $method = 'row';
        }
        else if($num_rows == TRUE)
        {
            $method = 'num_rows';
        }
        else if($single == TRUE)
        {
            $method = 'row';
        }
        else
        {
            $method = 'result';
        }
        
        $query_str = '';
        // Order by
        if( ! (strstr($query_str, 'ORDER BY')) && $num_rows == FALSE)
        {
            $this->db->order_by($this->_order_by);
        }
        
        if($limitFrom != NULL && $num_rows == FALSE)
        {
            $this->db->limit($limitFrom, $limitTo);
        }
        
        return $this->db->get($this->_table_name)->$method();
    }
    
    /**
     * Default SELECT action from database by data
     * 
     * @param array $where
     * @param boolean $single
     * @param boolean $num_rows
     * @param int $limitFrom
     * @param int $limitTo
     * @return mixed
     */
    public function get_by($where, $single = FALSE, $num_rows = FALSE, $limitFrom = NULL, $limitTo = NULL)
    {
        $this->db->where($where);
        return $this->get(NULL, $single, $num_rows, $limitFrom, $limitTo);
    }
    
    /**
     * Default UPDATE action
     * 
     * @param array $data
     * @param int $id
     * @return boolean
     */
    public function update($data, $id = NULL)
    {
        // SET TIMESTAMP
        if($this->_timestamp == TRUE)
        {
            $now = date('Y-m-d G:i:s');
            if( ! $id) {
                $data['created'] = $now;
            }
            $data['modified'] = $now;
        }
        
        $filter = $this->_primary_filter;
        $id = $filter($id);
        
        foreach($data as $key => $value)
        {
            $this->_handle_null_active_record($data, $key);
        }
        $this->db->set($data);
        
        if($id)
        {
            $this->db->where($this->_primary_key, $id);
        }
        $res = $this->db->update($this->_table_name);
        
        if( ! $res)
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
     * Default INSERT action
     * 
     * @param array $data
     * @return boolean
     */
    public function insert($data = NULL)
    {
        // SET TIMESTAMP
        if($this->_timestamp == TRUE)
        {
            $now = date('Y-m-d G:i:s');
            $data['created'] = $now;
        }
        
        if($this->_primary_key != NULL)
        {
            $data[$this->_primary_key] = NULL;
        }
        
        $this->db->set($data);
        $this->db->insert($this->_table_name);
        $id = $this->db->insert_id();
        if($id > 0)
        {
            return $id;
        }
        else
        {
            return FALSE;
        }
    }
    
    /**
     * Default DELETE action by ID
     * 
     * @param int $id
     * @return boolean
     */
    public function delete($id)
    {
        $filter = $this->_primary_filter;
        $id = $filter($id);
        
        if(!$id)
        {
            return FALSE;
        }
        
        $this->db->where($this->_primary_key, $id);
        $this->db->limit(1);
        $res = $this->db->delete($this->_table_name);
        
        if( ! $res)
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
     * Default DELETE action by DATA
     * 
     * @param array $data
     * @param string $table_name
     * @return boolean
     */
    public function delete_by($data, $table_name = '')
    {        
        $action_table = ($table_name != '') ? $table_name : $this->_table_name;
        
        $this->db->where($data);
        $res = $this->db->delete($action_table);
        
        if( ! $res)
        {
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
     * Count result
     * 
     * @param array $where
     * @return type
     */
    public function count_result($where = NULL)
    {
        if($where != NULL)
        {
            $this->db->where($where);
        }
        
        return $this->db->count_all_results($this->_table_name);
    }
    
    /* 
     * Handles null values for active record data array.  If 
     * $dataarray[$data_key] is string 'NULL',
     * use active record set method to set null and unset the 
     * string 'NULL'.  $data_array is by reference.
     * @param - array   the data array
     * @param - data_key - string  the key to be evaluated
    */
    protected function _handle_null_active_record(&$data_array, $data_key)
    {
        if ('NULL' == $data_array[$data_key])
        {
            $this->db->set($data_key, null);
            unset($data_array[$data_key]);
        }
    }
}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */